package nars.derive.impl;

import jcog.Util;
import jcog.data.list.Lst;
import jcog.pri.PLink;
import nars.NAR;
import nars.derive.reaction.ReactionModel;
import nars.task.NALTask;
import org.eclipse.collections.impl.list.mutable.primitive.FloatArrayList;

import java.util.function.Consumer;

public class CompleteDeriver extends TaskBagDeriver {

    public CompleteDeriver(ReactionModel model, NAR nar) {
        super(model, nar);
    }

    private final Lst<NALTask> tasks = new Lst<>();
    private final FloatArrayList taskPri = new FloatArrayList() {
        /** fast clear; no need to zero the values */
        @Override
        public void clear() {
            size = 0;
        }
    };
    private final Consumer<PLink<NALTask>> queueTask = (x) -> {
        var t = x.id;
        var taskPri = t.pri();
        if (taskPri == taskPri) { //not deleted?
            this.tasks.addFast(t);
            this.taskPri.add(taskPri);
        }
    };

    @Override
    protected void __next(int iterTotal) {
        int iterMin = 8, iterMax = 12;
        try {
            var n = Math.min(iterTotal, focus.tasks.size()); //HACK this reinterprets the semantics of iterTotal
            tasks.ensureCapacity(n);
            focus.tasks.model.sampleUnique(n, rng, queueTask);
            double priSum = taskPri.sum();
            for (int i = 0, size = tasks.size(); i < size; i++) {
                var p = taskPri.get(i);
                var pRel = p / priSum;
                var iterations = (int) Util.lerpSafe(pRel, iterMin, iterMax);
                queue.runSeed(tasks.getAndNull(i), iterations);
            }

        } finally {
            tasks.clearFast();
            taskPri.clear();
        }
    }
}
