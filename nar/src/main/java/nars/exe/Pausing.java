package nars.exe;

@FunctionalInterface
public interface Pausing {
    void pause(boolean pause);
}
