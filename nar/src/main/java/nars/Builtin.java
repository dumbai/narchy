package nars;

import jcog.Str;
import jcog.data.map.UnifriedMap;
import nars.eval.Evaluation;
import nars.func.*;
import nars.subterm.Subterms;
import nars.table.BeliefTable;
import nars.table.TaskTable;
import nars.task.NALTask;
import nars.term.Compound;
import nars.term.Functor;
import nars.term.Termed;
import nars.term.atom.Atom;
import nars.term.atom.Bool;
import nars.term.atom.Int;
import nars.term.functor.AbstractInlineFunctor;
import nars.term.functor.AbstractInlineFunctor1;
import nars.term.functor.AbstractInlineFunctor2;
import nars.term.functor.LambdaFunctor;
import nars.term.obj.QuantityTerm;
import nars.term.util.Image;
import nars.term.util.Terms;
import nars.term.util.conj.Cond;
import nars.term.util.conj.CondDiff;
import nars.term.util.conj.ConjList;
import nars.term.util.transform.InstantFunctor;
import nars.term.util.var.DepIndepVarIntroduction;
import nars.term.var.Variable;
import nars.time.Tense;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeSet;
import java.util.function.BiFunction;
import java.util.stream.IntStream;

import static nars.Op.*;
import static nars.term.Functor.f0;
import static nars.term.atom.Bool.*;
import static nars.term.util.Image.imageNormalize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

/**
 * Built-in set of default Functors and Operators, registered into a NAR on initialization
 * Provides the standard core function library
 * <p>
 * see:
 * https:
 */
public enum Builtin {
    ;

    public static final Functor[] statik = {

            Cmp.cmp,

            MathFunc.add,
            MathFunc.mul,
            MathFunc.div,
            MathFunc.pow,

            MathFunc.mod,
            MathFunc.max,
            MathFunc.min,
//            MathFunc.lte,
//            MathFunc.gte,
//            MathFunc.lt,
//            MathFunc.gt,

            //MathFunc.xor,

            Member.member,

            Replace.replace,


            SetFunc.intersect,
            SetFunc.differ,
            SetFunc.union,


            ListFunc.append,
            ListFunc.reverse,
            ListFunc.sub,
            ListFunc.subs,

            new AbstractInlineFunctor2("noEvent") {
                @Override protected Term apply(Term c, Term e) {
                    return c.equals(e) ||
                            c instanceof Compound C && C.condOf(e) ? Null : c;
                }
            },
            new AbstractInlineFunctor2("noEventPN") {
                @Override protected Term apply(Term c, Term e) {
                    return c.equalsPN(e) ||
                            (c instanceof Compound C) && C.condOf(e, 0) ? Null : c;
                }
            },

            new AbstractInlineFunctor1.AbstractInstantFunctor1("unneg") {
                @Override
                protected Term apply1(Term x) {
                    return x.unneg();
                }
            },

            new AbstractInlineFunctor1.AbstractInstantFunctor1("imageNormalize") {
                @Override
                protected Term apply1(Term x) {
                    return imageNormalize(x);
                }
            },

            Functor.f2Inline("imageInt", Image::imageInt),
            Functor.f2Inline("imageExt", Image::imageExt),


            new AbstractInlineFunctor2("eventOf") {
                @Override
                protected Term apply(Term conj, Term event) {
                    return conj instanceof Compound C && C.condOf(event) ? True : False;
                }
            },

            /** removes one matching event, chosen at random  */
            new AbstractInlineFunctor2(atom("condWithoutAny")) {
                @Override
                @Deprecated protected Term apply(Term conj, Term event) {
                    return CondDiff.diffAny(conj, event, false);
                }
            },

            /** removes one matching event, chosen at random, pos or negated */
            new AbstractInlineFunctor2(atom("condWithoutAnyPN")) {
                @Override
                @Deprecated protected Term apply(Term conj, Term event) {
                    return CondDiff.diffAny(conj, event, true);
                }
            },
            new AbstractInlineFunctor2(atom("condIntersect")) {
                @Override
                @Deprecated protected Term apply(Term x, Term y) {
                    return Cond.intersect(x,y);
                }
            },

            /** removes all matching events */
            new AbstractInlineFunctor2(atom("conjWithoutFirst")) {
                @Override
                protected Term apply(Term conj, Term event) {
                    return CondDiff.diffFirst(conj, event);
                }
            },
            new AbstractInlineFunctor2(atom("conjWithoutFirstPN")) {
                @Override
                protected Term apply(Term conj, Term event) {
                    return CondDiff.diffFirstPN(conj, event);
                }
            },
            new AbstractInlineFunctor2(atom("condWithoutAll")) {
                @Override
                protected Term apply(Term conj, Term event) {
                    return CondDiff.diffAll(conj, event);
                }
            },

            /** removes all matching events, pos or negated */
            new AbstractInlineFunctor2(atom("condWithoutAllPN")) {
                @Override
                protected Term apply(Term conj, Term event) {
                    return CondDiff.diffAllPN(conj, event);
                }
            },

//            new AbstractInlineFunctor1("conjWithoutIndepEvents") {
//                @Override
//                protected Term apply1(Term conj) {
//                    if (!conj.CONJ() || !conj.hasAny(VAR_INDEP)) return conj;//unchanged
//                    ConjList c = new ConjList();
//                    boolean xternal = conj.dt() == XTERNAL;
//                    ((Compound) conj).events((when, what) -> {
//                        if (!what.hasAny(VAR_INDEP))
//                            c.add(when, what);
//                    }, xternal ? TIMELESS : 0, true, xternal);
//                    return c.term();
//                }
//            },

            /** applies the changes in structurally similar terms "from" and "to" to the target target */
            Functor.f3((Atom) $.atomic("replaceDiff"), (target, from, to) -> {
                if (from.equals(to))
                    return Null;

                int n;
                if (from.opID() == to.opID() && (n = from.subs()) == to.subs()) {

                    Map<Term, Term> m = null;
                    for (int i = 0; i < n; i++) {
                        Term f = from.sub(i), t = to.sub(i);
                        if (!f.equals(t)) {
                            if (m == null) m = new UnifriedMap<>(1);
                            m.put(f, t);
                        }
                    }
                    if (m != null) {
                        Term y = target.replace(m);
                        if (y != null && !y.equals(target))
                            return y;
                    }
                }
                return Null;
            }),

            /** similar to C/Java "indexOf" but returns a set of all numeric indices where the 2nd argument occurrs as a subterm of the first
             *  if not present, returns Null
             * */

            Functor.f2("indicesOf", (x, y) -> {


                int s = x.subs();
                if (s > 0) {
                    TreeSet<Term> indices = null;
                    for (int i = 0; i < s; i++) {
                        if (x.sub(i).equals(y)) {
                            if (indices == null) indices = new TreeSet<>();
                            indices.add(Int.i(i));
                        }
                    }
                    return indices == null ? Null : SETe.the(indices);
                }
                return Null;
            }),
            Functor.f2("keyValues", (x, y) -> {


                int s = x.subs();
                if (s > 0) {
                    TreeSet<Term> indices = null;
                    for (int i = 0; i < s; i++) {
                        if (x.sub(i).equals(y)) {
                            if (indices == null) indices = new TreeSet<>();
                            indices.add($.p(y, Int.i(i)));
                        }
                    }
                    if (indices == null)
                        return Null;
                    else {
                        return switch (indices.size()) {
                            case 0 -> Null;
                            case 1 -> indices.first();
                            default -> SETe.the(indices);
                        };
                    }
                }
                return Null;
            }),

            Functor.f2("varMask", (x, y) -> {
                int s = x.subs();
                if (s > 0) {
                    Term[] t = IntStream.range(0, s).mapToObj(i -> x.sub(i).equals(y) ? y : $.varDep("z" + i)).toArray(Term[]::new);
                    return $.p(t);
                }
                return Null;
            }),


            Functor.f1Const("reflect", reflect::reflect),


            Functor.f1Const("toString", x -> $.quote(x.toString())),
            Functor.f1Const("toChars", x -> $.p(x.toString().toCharArray(), $::the)),
            Functor.f1Const("complexity", x -> $.the(x.complexity())),

            flat.flatProduct,

            Functor.f2("similaritree", (a, b) ->
                    a instanceof Variable || b instanceof Variable ? null :
                            $.the(Str.levenshteinDistance(a.toString(), b.toString()))
            ),



            Functor.f2("ifThen", (condition, conseq) -> {
                if (!condition.equals(True)) {
                    if (condition == Null)
                        return Null;
                    return null;
                } else
                    return conseq;
            }),

            Functor.f3("ifThenElse", (condition, ifTrue, ifFalse) -> {
                if (!(condition instanceof Bool))
                    return null;
                if (condition == True)
                    return ifTrue;
                if (condition == False)
                    return ifFalse;

                return Null;
            }),

            Functor.f3("ifOrElse", (condition, conseqTrue, conseqFalse) -> {
                if (condition.hasVars())
                    return null;
                if (condition.equals(True))
                    return conseqTrue;
                if (condition.equals(False))
                    return conseqFalse;

                return Null;
            }),

            Functor.f2("ifNeqRoot", (returned, compareTo) ->
                    returned.equalsRoot(compareTo) ? Null : returned
            ),


            Functor.f2("subterm", (x, index) -> {
                try {
                    if (index.INT())
                        return x.sub($.intValue(index));
                } catch (NumberFormatException ignored) {
                }
                return null;
            }),

            Functor.f1Inline("quote", x -> x),

            Functor.f1Inline("condLatest", (Term x) -> {
                if (!x.SEQ())
                    return x;

                try (var e = ConjList.conds(x, false, false)) {
                    long l = e.whenLatest();
                    e.removeIf((w, z) -> w != l);
                    return e.term();
                }
            }),

            /** warning: this returns Null if unchanged */
            new AbstractInlineFunctor2("withoutPN") {
                @Override
                protected Term apply(Term container, Term _content) {
                    Term content = _content.unneg();
                    Term y = Terms.withoutAll(container, content::equalsPN);
                    return y == null || y.equals(container) ? Null : y;
                }
            },

            /** warning: this returns Null if unchanged */
            new AbstractInlineFunctor1("negateConds") {
                @Override protected Term apply1(Term x) {
                    return Cond.negateConds(x,
                        true
                        //!x.SEQ()
                        //false
                    );
                }
            },

            /** warning: this returns Null if unchanged */
            new AbstractInlineFunctor2("without") {
                @Override
                protected Term apply(Term container, Term content) {
                    Term y = Terms.withoutAll(container, content::equals);
                    return y == null || y.equals(container) ? Null : y;
                }
            },

            /** warning: this returns Null if unchanged */
            new AbstractInlineFunctor2("unsect") {
                @Override
                protected Term apply(Term container, Term content) {
                    Term y = Terms.withoutAll(container, content.CONJ() ? ((Compound) content).contains(false, false) : content::equals);
                    return y == null || y.equals(container) ? Null : y;
                }
            },

            Functor.f("slice", args -> {
                if (args.subs() != 2)
                    return null;

                Term x = args.sub(0);
                if (x.subs() > 0) {
                    int len = x.subs();

                    Term index = args.sub(1);
                    Op o = index.op();
                    if (o == INT) {

                        int i = Int.i(index);
                        return i >= 0 && i < len ? x.sub(i) : False;

                    } else if (o == PROD && index.subs() == 2) {
                        Term start = index.sub(0);
                        if (start.INT()) {
                            Term end = index.sub(1);
                            if (end.INT()) {
                                int si = Int.i(start);
                                if (si >= 0 && si < len) {
                                    int ei = Int.i(end);
                                    if (ei >= 0 && ei <= len) {
                                        if (si == ei)
                                            return EmptyProduct;
                                        if (si < ei) {
                                            return $.p(Arrays.copyOfRange(x.subterms().arrayClone(), si, ei));
                                        }
                                    }
                                }

                                return False;
                            }
                        }

                    }
                }
                return null;
            })
    };

//    private static Term nullIfEq(Term mustNotEqual, Term result) {
//        return mustNotEqual.equals(result) ? Null : result;
//    }

    private static final Map<Term, Functor> statiks;

    static {
        UnifriedMap<Term, Functor> s = new UnifriedMap<>(statik.length);
        for (Functor f : statik) s.put(f.term(), f);
        s.trimToSize();
        statiks = s;
    }

    public static @Nullable Functor functor(Term x) {
        return statiks.get(x);
    }

    private static void registerFunctorsStatic(NAR nar) {
        for (Functor t : statik)
            nar.add(t);
    }

    /**
     * instantiates NAR-local functors
     */
    private static void registerFunctorsDynamic(NAR nar) {
        nar.add(SetFunc.sort());

        /** dynamic target builder - useful for NAR specific contexts like clock etc.. */
        nar.add(new TermDynamic(nar));

        /** applies # dep and $ indep variable introduction if possible. returns the input term otherwise  */
        nar.add(Functor.f1Inline("varIntro", x -> {
            if (!(x instanceof Compound c)) return Null;
            Term result = DepIndepVarIntroduction.the.apply(c, nar.random(), null);
            return result != null ? result : Null;
        }));

/** TODO
         * replay(timeStart, timeEnd, [belief|goal|question] [pos|neg]) [!;]
         * functor context parameter to subsume Operator API as functors that trigger
         * async reactions when evaluated in imperative contexts
         */
        nar.add(new AbstractInlineFunctor2("replay") {

            @Override
            protected Term apply(Term a, Term b) {
                return null;
            }
        });


        nar.add(f1Table("beliefTruth", nar, BELIEF, (c, n) -> {
            long when = n.time();
            return $.quote(((BeliefTable) c).truth(when, when, n));
        }));
//        nar.on(Functor.f1Concept("goalTruth", nar, (c, n) -> $.quote(n.goal(c, n.time()))));

        nar.add(f0("self", nar::self));

        nar.add(Functor.f1("the", what -> {


            if (what instanceof Atom) {
                switch (what.toString()) {
                    case "sys" -> {
                        return $.p(
                                $.quote(nar.emotion.summary()),
                                $.quote(nar.memory.summary()),
                                $.quote(nar.emotion.summary()),
                                $.quote(nar.exe.toString())
                        );
                    }
                }
            }

            Termed x = nar.concept(what);
            if (x == null)
                x = what;

            return $.quote($.p($.quote(x.getClass().toString()), $.quote(x.toString())));
        }));


    }

    private static void registerOperators(NAR nar) {

        nar.add(atom("task"), (x, nn) -> {
            //TODO punctuation parameter: Task.BeliefAtom, Task.GoalAtom etc
            NALTask t = NALTask.taskEternal(x.term().sub(0).sub(0), BELIEF, $.t(1.0f, nn.confDefault(BELIEF)), nn);
            t.withPri(nn.priDefault(BELIEF));
            nn.input(t);
            return null;
        });

        nar.addOp1("assertTrue", (x, nn) -> {
            if (x.hasVars()) //???
                assertSame(True, x);
        });

        //TODO separate into assert and equals functors
        nar.addOp2("assertEquals", (x, y, nn) -> {
            if (!x.equals(y) && !x.hasVars() && !y.hasVars())
                assertEquals(/*msg,*/ x, y);
        });

    }


    public static void init(NAR nar) {
        registerFunctorsStatic(nar);
        registerFunctorsDynamic(nar);
        registerOperators(nar);
    }

    public static LambdaFunctor f1Table(String termAtom, NAR nar, byte punc, BiFunction<TaskTable, NAR, Term> ff) {
        assert punc == BELIEF || punc == GOAL;
        boolean beliefOrGoal = punc == BELIEF;
        return Functor.f1(atom(termAtom), t -> {
            TaskTable c = nar.table(t, beliefOrGoal);
            return c != null ? ff.apply(c, nar) : null;
        });
    }

    private static final class TermDynamic extends AbstractInlineFunctor implements InstantFunctor<Evaluation> {

        private final NAR nar;

        TermDynamic(NAR nar) {
            super(TERM);
            this.nar = nar;
        }

        @Override
        public Term apply(Evaluation e, Subterms s) {
            Term opTerm = s.sub(0);
            Op o = Op.op((Atom) opTerm);
            if (o == null)
                return Null; //TODO throw

            int dt;
            if (s.subs() == 3) {
                if (!o.temporal)
                    throw new UnsupportedOperationException("unrecognized modifier argument: " + s);

                Term dtTerm = s.sub(2);
                if (!(dtTerm instanceof QuantityTerm)) {
                    dtTerm = QuantityTerm.the(dtTerm);
                    if (dtTerm == null)
                        return Null;
                }

                dt = Tense.occToDT(nar.time.toCycles(((QuantityTerm) dtTerm).quant));

            } else
                dt = DTERNAL;

            return o.the(dt, s.sub(1).subterms().arrayShared());
        }
    }



}