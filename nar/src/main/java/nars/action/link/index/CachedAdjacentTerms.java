package nars.action.link.index;

import jcog.Util;
import jcog.pri.PriReference;
import jcog.pri.bag.Sampler;
import nars.NAL;
import nars.Term;
import nars.concept.snapshot.Snapshot;
import nars.derive.Deriver;
import nars.focus.Focus;
import nars.link.TermLinkBag;
import org.jetbrains.annotations.Nullable;

import java.util.Iterator;
import java.util.random.RandomGenerator;

import static jcog.Util.lerpSafe;
import static jcog.pri.op.PriMerge.max;
import static nars.NAL.premise.TERMLINK_CAP_MAX;
import static nars.NAL.premise.TERMLINK_CAP_MIN;

/**
 * caches ranked reverse atom termlinks in concept meta table
 * stateless
 */
public class CachedAdjacentTerms extends AdjacentTerms {

	/** Scope
	 *    true: Global (shared by all Focus's),
	 *    false: per Focus, Focus-local */
	private final boolean global;

	public CachedAdjacentTerms(boolean global) {
		this.global = global;
	}

	@Nullable @Override
	public Iterator<PriReference<Term>> adjacencies(Term from, Term to, Deriver d) {
        var f = d.focus;

		TermLinkBag cached = Snapshot.get(
			id(f), to,
			(int) updatePeriod(to, f),
			(c, b) ->
				commit(b == null ? new TermLinkBag() : b, c.term(), d),
			f);

		return cached == null || cached.isEmpty() ? null : _adjacencies(from, d.rng, cached);
	}

	private TermLinkBag commit(TermLinkBag bag, Term x, Deriver d) {
		return commit(bag, x, d.focus, d.rng);
	}

	private TermLinkBag commit(TermLinkBag b, Term x, Focus f, RandomGenerator rng) {
		var src = f.links;
		b.capacity(capacity(x, f));
		var tries = tries(x, src.capacity());
		if (tries > 0)
			b.commit(x,
				src.sampleUnique(rng),
				Math.min(src.size(), tries),
				pri(x, b.capacity()),
				NAL.derive.TERMLINK_and_CLUSTERING_FORGET_RATE);
		return b;
	}

	/** insertion priority */
	private float pri(Term t, int capTgt) {
		if (TermLinkBag.Merge == max)
			return 0.5f;
		else {
			//merge == plus
			return (float) Math.pow(1 + capTgt,
					-1 / 2f
					//-1/4f
			);
			//return 1 / Math.sqrt(1 + capTgt);
			//return (capTgt / (1f + src.capacity()));
		}
	}

	private Iterator<PriReference<Term>> _adjacencies(Term e, RandomGenerator rng, Sampler<PriReference<Term>> b) {
		return b.sampleUnique(rng);
		//((Bag)b).print(); System.out.println();
		//return PriReference.get(b.sampleUnique(rng));
	}

	/** cache update period, in cycles */
	private float updatePeriod(Term to, Focus f) {
		return f.durSys;
		//return to.volume() * f.dur();
		//return to.volume() * d.nar.dur();
	}

	/** cache capacity per concept,bag */
	public int capacity(Term x, Focus f) {
		//TODO proportional to focus capacity
		return Math.round(lerpSafe(
				Util.unitizeSafe(((float)x.complexity()) / f.complexMax()),
				TERMLINK_CAP_MAX,
				TERMLINK_CAP_MIN));
	}

	/** bag scan rate, in max # of items to test from Bag */
	public int tries(Term x, int srcCap) {
		return Math.round(srcCap * NAL.premise.TERMLINK_DENSITY);
		//return Math.round(Fuzzy.mean((float)capTgt, srcCap) * NAL.premise.termLinkTrying);
		//return capTgt;
		//return Math.max(1, (int) Util.sqrt(capTgt));
		//return Integer.MAX_VALUE; //exhaustive
		//return b.size()/2; //semi-exhaustive
		//return Math.max(1, (int) Util.sqrt(b.capacity()));
	}

	private static final String idGlobal = CachedAdjacentTerms.class.getSimpleName();

	private String id(Focus f) {
		return global ? idGlobal : f.key(this, idGlobal, f.id);
	}


//	/** experimental */
//	private double priHebbian(Term t, int capTgt, Bag<Premise, Premise> src) {
//		/* hebbian-like 'fire-together': use the priority of the concept to scale the priorities of termlinks */
//		var tgtLink = src.get(MutableTaskLink.link(t));
//		float tgtPri = tgtLink!=null ? tgtLink.priElseZero() : 0;
//		float bagMax = src.priMax(); //normalizing factor
//		if (bagMax > Prioritized.EPSILON) tgtPri = Math.min(1, tgtPri / bagMax);
//		return Util.max(1.0/ capTgt, tgtPri);
//	}
}