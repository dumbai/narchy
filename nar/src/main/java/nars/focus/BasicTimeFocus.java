package nars.focus;

import jcog.signal.FloatRange;

import static java.lang.Math.round;

public class BasicTimeFocus extends TimeFocus {

    /**
     * duration of present-moment perception, in global clock cycles,
     * specific to this What, and freely adjustable
     * TODO DurRange - with .fps() method
     */
    public final FloatRange dur = new FloatRange(1, 0, 128 * 1024);

    /**
     * past/present/future shift, in system durations
     */
    public final FloatRange shift = new FloatRange(0, -8 * 1024, +8 * 1024);

    /** 1 is flat, past=future */
    public final FloatRange pastShiftMultiplier = new FloatRange(1, 0, 4);


//    /** align to dur's */ private boolean ditherDur = true;

    public final float dur() {
        return dur.floatValue();
    }

    public final void dur(float nextDur) {
        dur.setSafe(nextDur);
    }

    public final void shiftDurs(float durs) {
        shift.set(durs);
    }

    @Override
    public final long[] when(Focus f) {
        return when(
        f.time() + round(shift(shift.doubleValue(), dur.floatValue())),
            dur());
    }

    private double shift(double shift, float dur) {
        //return shift * dur;

        return (shift >= 0 ? shift : (shift * pastShiftMultiplier.asFloat())) * dur;
    }

}