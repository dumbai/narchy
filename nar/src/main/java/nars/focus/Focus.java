package nars.focus;

import jcog.TODO;
import jcog.Util;
import jcog.event.ByteTopic;
import jcog.event.Off;
import jcog.event.Topic;
import jcog.memoize.MemoGraph;
import jcog.pri.PLink;
import jcog.pri.Prioritizable;
import jcog.pri.UnitPri;
import jcog.pri.bag.Bag;
import jcog.signal.FloatRange;
import jcog.signal.IntRange;
import jcog.util.ConsumerX;
import nars.*;
import nars.concept.Concept;
import nars.concept.TaskConcept;
import nars.derive.pri.Budget;
import nars.derive.pri.DefaultBudget;
import nars.eval.Evaluator;
import nars.eval.TaskEvaluation;
import nars.focus.util.TaskAttention;
import nars.input.DirectTaskInput;
import nars.input.TaskInput;
import nars.link.TaskLink;
import nars.link.TaskLinkArrayBag;
import nars.task.CommandTask;
import nars.task.NALTask;
import nars.task.SerialTask;
import nars.term.Termed;
import nars.time.Tense;
import org.jetbrains.annotations.Nullable;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.*;
import java.util.random.RandomGenerator;
import java.util.stream.Stream;

import static nars.Op.TIMELESS;
import static nars.term.Functor.isFunc;

public class Focus extends NARPart implements Externalizable, ConsumerX<Task> {

    /**
     * termlinks - associative attention focus
     */
    public final Bag<TaskLink, TaskLink> links;
    public final TaskAttention tasks;

    /**
     * capacity of the links bag
     */
    public final IntRange linkCapacity = new IntRange(0, 0, 4096) {
        @Override protected void changed(int next) {
            links.setCapacity(next);
        }
    }, taskCapacity = new IntRange(0, 0, 2048) {
        @Override protected void changed(int next) {
            tasks.setCapacity(next);
        }
    };

    public final ByteTopic<Task> eventTask = new ByteTopic<>(Op.Punctuation);
    public final PriSource pri;
    public final UnitPri freq = new UnitPri(0.5f);
    /**
     * local metadata
     */
    public final Map meta = new ConcurrentHashMap<>();
    private final FloatRange commitDurs = new FloatRange(1, 0.5f, 16);
    /**
     * ensure at most only one commit is in progress at any time
     */
    private final AtomicBoolean busy = new AtomicBoolean(false);

    public Function<Bag, Consumer<Prioritizable>> updater =
        new BagForget();
        //new BagSustainByVariables();

    /**
     * accepts incoming tasks
     * buffering allows deduplicating same results from different threads without otherwise having to input all into belief tables directly
     */
    public TaskInput input =
        new DirectTaskInput() //low latency, low throughput
        //new MapTaskInput() //provides some dedupe
        //new BagTaskInput(1, 1/3f) //WARNING bag seems to become inconsistent for unknown reason
    ;

    private final Consumer<NALTask> ACTIVATE = this::activate;
    private final BiConsumer<NALTask,TaskConcept> REMEMBER = this::_remember;

    public TimeFocus time = new BasicTimeFocus();
    public Budget budget = new DefaultBudget();

    public transient float durSys = 1;

    //public final IntRange volMax = new IntRange(Inte, );
    private int volMax = Integer.MAX_VALUE;

    /** update period timing */
    private volatile long prev, now, updateNext, novelBefore;

    public Focus(Term id, int linkCapacity, int taskCapacity) {
        this(id,
            new TaskLinkArrayBag().buffered()
        );
        this.linkCapacity.set(linkCapacity);
        this.taskCapacity.set(taskCapacity);
    }


    public Focus(Term id, Bag<TaskLink, TaskLink> links) {
        super(id);
        this.pri = new PriSource(this.id, 1);
        this.links = links;
        this.tasks = new TaskAttention(NAL.TASKBAG_PUNC);

        input.start(this);
    }

    @Override
    protected void starting(NAR nar) {

        durSys = nar.dur();

        if (time.dur() <= 0) time.dur(durSys); //HACK for dur=0

        updateNext = nar.time() - 1; //force start ASAP
        super.starting(nar);

        nar.pri.add(pri);

        tasks.init(this);
    }

    public final boolean link(TaskLink x) {
        return links.put(x) !=null;
    }

    public void clearLinks() {
        links.clear();
        //links.clearSoft();
    }

    /** concepts in links */
    public Stream<Concept> concepts() {
        return terms(/*Term::CONCEPTUALIZABLE*/s -> true)
                .map(nar::concept)
                .filter(Objects::nonNull);
    }

    /** terms in links */
    public final Stream<Term> terms(Predicate<Term> filter) {
        return links.stream()
                .unordered()
                .flatMap(x -> Stream.of(x.from(), x.to()))
                .distinct()
                .filter(filter);
    }

    @Override
    public final void writeExternal(ObjectOutput objectOutput) {
        throw new TODO();
    }

    @Override
    public final void readExternal(ObjectInput objectInput) {
        throw new TODO();
    }

    @Override
    protected void stopping(NAR nar) {
        nar.pri.remove(pri);
        super.stopping(nar);
    }

    /**
     * perceptual duration (cycles)
     */
    public final float dur() {
        return time.dur();
    }

    public void log(Task t) {
        eventTask.emit(t, t.punc());
        nar.emotion.busyVol.add(t.term().complexity());
    }

    @Deprecated
    public final RandomGenerator random() {
        return nar.random();
    }

    public final void remember(NALTask x) {
        input.accept(x);
    }
    private void _remember(NALTask x, TaskConcept c) {
        input.accept(x, c);
    }

    private void run(CommandTask t) {
        if (isFunc(t.term()))
            new CommandEvaluation(t);
        else
            throw new UnsupportedOperationException();
    }

    public final Off onTask(Consumer<Task> listener, byte... punctuations) {
        return eventTask.on(listener, punctuations);
    }

    public final boolean commit(long now) {
        if (now >= this.updateNext) {
            if (Util.enterAlone(busy)) {
                try {
                    _commit(now);
                    return true;
                } finally {
                    Util.exitAlone(busy);
                }
            }
        }
        return false;
    }

    private void _commit(long now) {
        commitTime(now);

        budget.accept(this);

        links.commit(updater.apply(links));

        input.commit();
    }


    private void commitTime(long now) {
        this.prev = this.now;
        this.now = now;
        this.durSys = nar.dur();

        var durCommit = Math.max(1, durSys * commitDurs.floatValue());
        this.updateNext = now + Math.round(durCommit);

        float durNovel =
            durCommit;
            //durSys;
            //dur();
        long cycNovel = Math.max(1, Tense.occToDT(Math.round(durNovel * NAL.belief.NOVEL_DURS)));
        novelBefore = now - cycNovel;
    }

    public Off log() {
        return log((Predicate) null);
    }

    public Off log(@Nullable Predicate<?> pred) {
        return logTo(System.out, pred);
    }

    public Off logTo(Appendable out, @Nullable Predicate<?> includeValue) {
        return logTo(out, null, includeValue);
    }

    private Off logTo(Appendable out, @Nullable Predicate<String> includeKey, @Nullable Predicate includeValue) {
        return eventTask.on(new TaskLogger(includeValue, out, includeKey));
    }

    //@Override
    public final float pri() {
        return pri.pri();
    }

    public final float freq() {
        return freq./*pri*/asFloat();
    }

    public final Focus pri(float freq, float pri) {
        freq(freq);
        this.pri.pri(pri);
        return this;
    }

    public final Focus freq(float f) {
        freq.pri(f);
        return this;
    }

    @Nullable
    public <X> X local(Object key) {
        return (X) meta.get(key);
    }

    public <K, X> X local(K key, X value) {
        var v2 = meta.put(key, value);
        return (X)(v2 != null ? v2 : value);
    }

    public <K, X> X local(K key, Function<K, ? extends X> build) {
        return (X) meta.computeIfAbsent(key, build);
    }

    public <C> Stream<C> localInstances(Class<? extends C> valueClass) {
        return meta.entrySet().stream()
                .map(i -> ((Map.Entry)i).getValue())
                .filter(valueClass::isInstance);
    }

    /**
     * novelty filter for: activation, logging, etc.
     * determines whether a previously-stored task is novel, and can be re-activated now
     *
     * @return TIMELESS if it should not be reactivated, or the current time to set its
     * re-creation time as if it should be.
     */
    public boolean novelTime(NALTask x) {
        long creation;
        if (NAL.TASK_ACTIVATE_ALWAYS || ((creation = x.creation()) == TIMELESS || creation <= novelBefore)) {
            x.setCreation(time()); //initialize or re-activate
            return true;
        } else
            return false;
    }

    @Override
    public final void accept(Task x) {
        if (x instanceof NALTask X)
            remember(X); //rememberNow(X);
        else if (x instanceof CommandTask C)
            run(C);
        else
            throw new UnsupportedOperationException();
    }

    public final void acceptAll(Task... t) {
        for (Task x : t) accept(x);
    }

    public final void acceptAll(Stream<? extends Task> t) {
        t.forEach(this);
    }

//    /**
//     * where threads can store threadlocal
//     */
//    private final ThreadLocal<Map> threadLocal = ThreadLocal.withInitial(HashMap::new);
//
//    public final <X> X threadLocal(Object key) {
//        return (X) threadLocal.get().get(key);
//    }
//
//    public final <X> X threadLocal(Object key, Function<Focus, X> ifMissing) {
//        //return (X) threadLocal.get().computeIfAbsent(key, k -> ifMissing.apply(this));
//
//        var m = threadLocal.get();
//        Object v = m.get(key);
//        if (v == null)
//            m.put(key, v = ifMissing.apply(this));
//        return (X) v;
//    }

    public final Focus dur(float dur) {
        time.dur(dur);
        return this;
    }

    public final long[] when() {
        return time.when(this);
    }


    public int complexMax() {
        return Math.min(volMax, nar.complexMax());
    }



//    private void _activate(Remember r) {
//        activate(r.target(), r.stored.punc(), r.stored.priElseZero());
//    }

//    public final void activate(Term x, byte punc, float p) {
//        link(MutableTaskLink.link(x).priPunc(punc, activation(x, punc, p)));
//    }

    /** time since last update, in durs */
    public final float updateDT() {
        return (now - prev)/durSys;
    }


    /** time of the latest update */
    public final long time() {
        return now;
    }

    /** current attention priority of the focus within the running system */
    public final float focusPri() {
        PLink<Focus> f = nar.focus.get(id);
        return f!=null ? f.pri() : Float.NaN;
    }

    /** utility class for caching a shared key constructed from 2 components: prefix and id */
    public String key(Object key, String prefix, Object id) {
        String k = local(key);
        return k != null ? k : local(key, prefix + "," + id);
    }

    /** sets a specific max volume for this Focus.  volMax(Integer.MAX_VALUE) to disable */
    public final void complexMax(int v) {
        volMax = v;
    }

    public Off onTask(String key, Consumer<Task> each, byte... punc) {
        Off on;
        if ((on = (Off) meta.get(key)) == null) {
            meta.put(key, on = onTask(each, punc));
        }
        return on;
    }

    public void addLaterStaged(NALTask x, MemoGraph g) {
        if (x instanceof SerialTask)
            g.once(x, ACTIVATE);
        else
            g.chain(x, PRE_REMEMBER, REMEMBER);
    }


    /**
     * TODO histogram priority to adaptively determine ANSI color by percentile
     */
    static class TaskLogger implements Consumer<Task> {

        private final @Nullable Predicate includeValue;
        private final Appendable out;
        String previous;

        TaskLogger(@Nullable Predicate includeValue, Appendable out, @Nullable Predicate<String> includeKey) {
            this.includeValue = includeValue;
            this.out = out;
            previous = null;
            Topic.all(this, (k, v) -> {
                if (includeValue == null || includeValue.test(v)) {
                    outputEvent(out, previous, k, v);
                    previous = k;
                }
            }, includeKey);
        }

        private static void outputEvent(Appendable out, String previou, String chan, Object v) {

            try {

                if (!chan.equals(previou)) {
                    out.append(chan).append(": ");

                } else {

                    int n = chan.length() + 2;
                    for (int i = 0; i < n; i++)
                        out.append(' ');
                }

                if (v instanceof Object[] va) {
                    v = Arrays.toString(va);
                } else if (v instanceof Task tv) {
                    v = tv.toString(true);
//                    float tvp = tv.priElseZero();
//                    v = ansi()
//                            .a(tvp >= 0.25f ?
//                                    Ansi.Attribute.INTENSITY_BOLD :
//                                    Ansi.Attribute.INTENSITY_FAINT)
//                            .a(tvp > 0.75f ? Ansi.Attribute.NEGATIVE_ON : Ansi.Attribute.NEGATIVE_OFF)
//                            .fg(budgetSummaryColor(tv))
//                            .a(
//                                    v
//                            )
//                            .reset()
//                            .toString();
                }

                out.append(v.toString()).append('\n');
            } catch (IOException e) {
                logger.error("outputEvent", e);
            }

        }

//        static Ansi.Color budgetSummaryColor(Prioritized tv) {
//            return switch ((int) (tv.priElseZero() * 5)) {
//                default -> Ansi.Color.DEFAULT;
//                case 1 -> Ansi.Color.MAGENTA;
//                case 2 -> Ansi.Color.GREEN;
//                case 3 -> Ansi.Color.YELLOW;
//                case 4 -> Ansi.Color.RED;
//            };
//        }

        @Override
        public void accept(Task v) {
            if (includeValue == null || includeValue.test(v)) {
                outputEvent(out, previous, "task", v);
                previous = "task";
            }
        }


    }

    private class CommandEvaluation extends TaskEvaluation<Task> {

        private final CommandTask task;

        CommandEvaluation(CommandTask t) {
            super(new Evaluator(nar::axioms));
            this.task = t;
            eval(t.term());
        }

        @Override
        public CommandTask task() {
            return task;
        }

        @Override
        public NAR nar() {
            return nar;
        }

        @Override
        public void accept(Task x) {
            Focus.this.accept(x);
        }

        @Override
        public boolean test(Term term) {
            return false; //first only
        }
    }

//    @Override
//    public void remember(NALTask x, Premise premise) {
//        super.remember(x, premise);
//
//        float reflect = budget.deriveTaskReflect.floatValue();
//        if (reflect > 0 && premise instanceof NALPremise n) {
//            float p = x.priElseZero();
//            float pReflect = p * reflect;
//            if (pReflect > Prioritized.EPSILON) {
//                byte xPunc = x.punc();
//                if (premise instanceof NALPremise.DoubleTaskPremise d) {
//                    //activate(d.from(), xPunc, pReflect/2);
//                    //activate(d.to(), xPunc, pReflect/2);
//                    activate(d.from(), xPunc, pReflect/2);
//                } else {
//                    activate(n.from(), xPunc, pReflect);
//                }
//            }
//        }
//    }

    private final BiFunction<MemoGraph, NALTask, TaskConcept> PRE_REMEMBER =
        (G, _x) ->
                G.share(_x, (GG, __x) ->
                        GG.share(__x.term().concept(), this::conceptualize));


    private TaskConcept conceptualize(Termed x) {
        return nar.conceptualizeTask(x);
    }
    private void activate(NALTask t) {
        input.activate(t);
    }

}