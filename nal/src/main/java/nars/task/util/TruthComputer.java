package nars.task.util;

import nars.truth.Truth;

public interface TruthComputer {
    Truth computeTruth();
}