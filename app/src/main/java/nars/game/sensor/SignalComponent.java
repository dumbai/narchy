package nars.game.sensor;

import nars.NAR;
import nars.Term;
import nars.game.Game;

/** one of potentially several component signals in a vector sensor */
public abstract class SignalComponent extends SignalConcept {

	protected SignalComponent(Term componentID, NAR nar) {
		super(componentID, true, nar);
	}

	public float value;

	public float updateValue(Game g) {
		return this.value = value(g);
	}
	protected abstract float value(Game g);

}