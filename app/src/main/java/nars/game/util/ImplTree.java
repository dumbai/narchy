package nars.game.util;

import jcog.Is;
import jcog.Research;
import jcog.data.list.Lst;
import jcog.pri.PLink;
import nars.NAR;
import nars.Term;
import nars.concept.Concept;
import nars.concept.snapshot.Snapshot;
import nars.focus.Focus;
import nars.link.TermLinkBag;
import nars.task.NALTask;
import nars.term.Compound;

import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

/** untested */
@Research @Is({"Crystallization","Program synthesis","Program_derivation"})
public class ImplTree  {


    private final Lst<NALTask> impl = new Lst();

//    @Deprecated private final NAR nar;


    private final Term target;

    private boolean equalOrCondOf = true;

//    private final int iDur;
//    private transient int dtDither = 0;
//    private final int volMax = Integer.MAX_VALUE;
//    private final double eviMin = NAL.truth.EVI_MIN;

    public ImplTree(Term target) {
        this.target = target;
//        dtDither = n.dtDither();
//        this.nar = n;
//        iDur = (int) Math.ceil(nar.dur() /2);
    }

//    @Deprecated public static void want(Term t, int condCap, Game g) {
//        plan(t, null, null, condCap, g);
//    }
//
//    @Deprecated public static void need(Term t, Truth goalTruth, long[] goalStamp, int condCap, Game g) {
//        plan(t, goalTruth, goalStamp, condCap, g);
//    }

//    @Deprecated private static void plan(Term t, @Nullable Truth goalTruth, @Nullable long[] goalStamp, int condCap, Game g) {
//
//        float durLoop = g.dur();
//
//        Focus f = g.focus();
//
//        float dur =
//                //durLoop;
//                g.durFocus();
//
//        long start = f.now + Math.round(durLoop /2);
//        long   end = start + (int) Math.ceil(dur);
//        int dtDither = f.nar.dtDither();
//        start = Tense.dither(start, dtDither);
//        end   = Tense.dither(end,   dtDither);
//
//        boolean global = false; //TODO ?
//        boolean fwd = goalTruth==null;
//
//        ImplTree i = new ImplTree(t);
//        i.equalOrCondOf = !fwd;
//        if (global)
//            i.match(f.nar); //TODO cache
//        else
//            i.match(f, true);
//
//
//        Stream<NALTask> tt =
//            fwd ?
//                i.want(start, end, dur, f, condCap)
//                :
//                i.need(t, start, end, dur, goalTruth /* projected? */, goalStamp, f, condCap)
//                ;
//
//
//        f.remember(tt);
//    }

//    @Deprecated public static void plan(int condCap, Game g) {
//
//        float durLoop = g.dur();
//
//        Focus f = g.focus();
//
//        float dur =
//                //durLoop;
//                g.durFocus();
//
//        long s = f.now + Math.round(durLoop /2);
//        long   e = s + (int) Math.ceil(dur);
//        int dtDither = f.nar.dtDither();
//        s = Tense.dither(s, dtDither);
//        e   = Tense.dither(e,   dtDither);
//
////        boolean global = false; //TODO ?
//
////        ImplTree i = new ImplTree(t);
////        i.equalOrCondOf = !fwd;
////        if (global)
////            i.match(f.nar); //TODO cache
////        else
////            i.match(f, true);
//
//
//        ImplTaskify i = new ImplTaskify(s, e, dur, f.nar, condCap);
//        f.remember(i.tasks((((BagFocus)f).bag.stream()), f));
//    }


    public ImplTree match(NAR n /* TODO cache */) {
        return match(n.tasks(true,false,false,false));
    }


    public ImplTree match(Focus f, boolean cache) {
        NAR nar = f.nar;
        var t = cache ?
            cache(getClass().getSimpleName() + ":" + f.term() /* TODO direction */, ()->matchTerms(f), f.time(), Math.round(f.durSys), f.nar) :
            matchTerms(f);

        return _match(t
            .map(nar::conceptualizeDynamic)
            .filter(Objects::nonNull)
            .flatMap(c -> c.beliefs().taskStream()));
    }

    private Stream<Term> cache(String ctx, Supplier<Stream<Term>> m, long now, int ttl, NAR n) {
        int capacity = 64; //TODO
        float priEach = 0.5f; //1f/CAP; //TODO
        BiFunction<Concept, TermLinkBag, TermLinkBag> u = (c, b) -> {
            if (b == null) b = new TermLinkBag();
            b.capacity(capacity);
            TermLinkBag B = b;
            m.get().forEach(z -> B.put(new PLink<>(z, priEach)));
            B.commit();
            return B;
        };
        TermLinkBag b = Snapshot.get(ctx, target, now, ttl, u, n);
        return b!=null ? b.stream().map(Supplier::get) : Stream.empty();
    }

    private Stream<Term> matchTerms(Focus f) {
        //Stream<Premise> fs = Streams.stream(f.iterator());
        var fs = f.links.stream();
        return fs
                //.filter(Premise::self).map(Premise::from)
                .flatMap(z -> z.self() ? Stream.of(z.from()) : Stream.of(z.from(), z.to()))
                    .filter(Term::TASKABLE)
                    //.map(Term::concept)
                    .distinct()
                .filter(termMatch());
    }

    public ImplTree match(Stream<NALTask> n) {
        return _match(n.filter(beliefTermFilter()));
    }

    private ImplTree _match(Stream<NALTask> n) {
        n.forEach(this::add);
        return this;
    }

    private Predicate<? super NALTask> beliefTermFilter() {
        Predicate<Term> tf = termMatch();
        return t -> t.BELIEF() && tf.test(t.term());
    }

    private Predicate<Term> termMatch() {
        return termMatch(target);
    }

    private Predicate<Term> termMatch(Term target) {
        Predicate<Term> e = target.equals();
        Predicate<Term> p = equalOrCondOf ? e : (x)->x instanceof Compound ? ((Compound)x).condOf(target, 0) : e.test(x);
        return tt -> {
            if (!tt.IMPL()) return false;
            if (!p.test(tt.sub(1))) return false;

            Term su = tt.subUnneg(0);
            if (!su.TASKABLE() || su.IMPL() /* HACK */) return false;
            if (e.test(su)) return false; //repeats or inverts predicate

            return true;
        };
    }

    private void add(NALTask t) {
        impl.add(t);
    }

//    public void print(PrintStream out) {
//        out.println(pred);
//        impl.valuesView().toSortedList(taskCmp).forEach(v ->{
//        //impl.forEachKeyValue((k,v)->{
//            out.println(v);
//        });
//        out.println();
//    }

//    /** determine factors
//     *
//     * emulates:
//     *   (  A ==> C), (  B ==> C), --var({A,B}), neq(A,B), --conjSequence(A), --conjSequence(B) |- ( (A||B) ==> C), (Belief:Mix, Time:Mid)
//     *
//     * impltree ->
//     *   select slice of time to compress
//     *   for each unique subject
//     *      if > 1 tasks, compress all the common-termed tasks using a variant of EternalTable
//     *      to as few tasks as possible by setting capacity to 1
//     *      but disabling discarding
//     *   for each N remaining tasks,
//     *   compute NxN subj ||'s, yielding Disjunction-Normal-Form-like expression trees (no vol limit while computing)
//     *   from remaining tasks, apply possible factoring
//     *   filter weak and voluminous tasks
//     *   input result as eternalized rule
//     *
//     *
//     * */
//    public Lst<NALTask> disjunctify(long s, long e, float dur, long now, int dtMin, int dtMax, int dtTarget) {
//
//
//        ArrayHashSet<NALTask> tasks = impl(dtMin, dtMax);
//        if (tasks.isEmpty()) return new Lst<>(0) /*EMPTY_LIST*/;
//
//        int n = tasks.size();
//        if (n <= 1) return tasks.list;
//
//        float confMin = (float) e2c(eviMin);
//
//        List<NALTask> merged = new Lst<>();
//        do {
//            merged.clear();
//
//            tasks.list.sort(taskComparator(s, e, dur));
//            if (tasks.size() > capacity) {
//                ListIterator<NALTask> ii = tasks.listIterator(capacity);
//                while (ii.hasNext()) {
//                    ii.next();
//                    ii.remove();
//                }
//                n = tasks.size();
//            }
//
//            main:
//            for (int i = 0; i < n - 1; i++) {
//                NALTask ac = tasks.get(i);
//                if (ac == null) continue;
//
//                Term a = ac.term().sub(0);
////                NALTask _ac = ac instanceof ProxyTask ? ((ProxyTask)ac).task : ac;
//
//                boolean aEternal = ac.ETERNAL();
//
//                final Truth at = ac.truthRelative(s, e, dur, now, 0, eviMin);
//
//                final StampOverlapping acOverlap = ac.stampOverlapping();
//
//                for (int j = i + 1; j < n; j++) {
//                    NALTask bc = tasks.get(j);
//                    if (bc == null || bc.ETERNAL()!=aEternal) continue;
//
//                    Term b = bc.term().sub(0);
////                    if (a.equalsPN(b.term())) continue; //ignore same term
//
////                    NALTask _bc = bc instanceof ProxyTask ? ((ProxyTask)bc).task : bc;
//                    if (acOverlap.test(bc)) continue;
//
//                    Term ab = Op.DISJ(a, b);
//                    if (!ab.EVENTABLE() || ab.volume() > volMax) continue;
//
////                    long[] se = Longerval.unionArray(ac.start(), ac.end(), bc.start(), bc.end());
////                    long s = se[0], e = se[1];
//
//                    final Truth bt = bc.truthRelative(s, e, dur, now, 0, eviMin);
//                    Truth t = NALTruth.Mix.apply(at, bt, confMin);
//                    if (t == null) continue;
//
//                    Term c = ac.term().sub(1);
//                    final Term abc = IMPL.the(ab, dtTarget, c);
//                    if (abc.TASKABLE()) {
//                        NALTask AB = NALTask.task(abc, BELIEF, t, s, e,
//                                Stamp.zip(ac, bc));
//                        tasks.remove(ac);
//                        tasks.remove(bc);
//                        Task.fund(AB, new Task[]{ac, bc}, z -> (float) (z == 0 ? ac.evi() : bc.evi()));
//                        merged.add(AB);
//                        break main;
//                    }
//                }
//            }
//            tasks.addAll(merged);
//        } while (!merged.isEmpty());
//
//        return tasks.list;
//    }

//    private ArrayHashSet<NALTask> impl(int dtMin, int dtMax) {
//        ArrayHashSet<NALTask> tasks = new ArrayHashSet<>();
//        impl.forEach(t -> {
//            Term tt = t.term();
//
//            int dt = tt.dt();
//            if (dt == DTERNAL) dt = 0;
//            int idt = -tt.sub(0).seqDur() + -dt;
//
//            if (idt >= dtMin && idt <= dtMax)
//                tasks.add(t);
//        });
//        return tasks;
//    }
//
//    private boolean dtFilter(int dt) {
//        int dt = tt.dt();
//        if (dt == DTERNAL) dt = 0;
//        int idt = -tt.sub(0).seqDur() + -dt;
//
//        return (idt >= dtMin && idt <= dtMax)
//            tasks.add(t);
//    }

    public boolean isEmpty() {
        return impl.isEmpty();
    }

//    /** backwards reward match */
//    @Deprecated public Stream<NALTask> need(Term t, long start, long end, float dur, Truth goalTruth, long[] goalStamp, Focus f, int condCap) {
//        return new ImplTaskify(start, end, dur, f.nar, condCap)
//            .need(t, impl.stream(), goalTruth, goalStamp, f);
//    }
//
//    /** forwards action match */
//    @Deprecated public Stream<NALTask> want(long start, long end, float dur, Focus f, int condCap) {
//        return new ImplTaskify(start, end, dur, f.nar, condCap)
//            .want(impl.stream(), f);
//    }


    //    public ImplTreeEvaluator truth(NAR n) {
//        final long now = n.time();
//        return truth(n, now, now + (int)Math.ceil(n.dur()));
//    }

}