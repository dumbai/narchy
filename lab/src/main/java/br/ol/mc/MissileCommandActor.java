package br.ol.mc;


import br.ol.mc.actor.Explosion;
import br.ol.mc.infra.Actor;
import br.ol.mc.infra.PixelCollider;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * MissileCommandActor class.
 * 
 * @author Leonardo Ono (ono.leo@gmail.com)
 */
public class MissileCommandActor extends Actor<MissileCommandGame> {
    
    private final MCComposite composite = new MCComposite();
    
    private Color overrideColor = Color.WHITE;
    private boolean useOverrideColor = true;
    private boolean automaticRandomColorUpdate;
    private double randomColorIndex;
    
    private boolean useXorColor;
    private Color xorColor = Color.BLACK;
    
    private BufferedImage frame;
    private BufferedImage[] frames;
    
    private boolean drawMask;
    private boolean affectedByMask;
    private boolean maskInverted;
    
    private PixelCollider collider;
    
    public MissileCommandActor(MissileCommandGame game) {
        super(game);
        collider = new PixelCollider(this);
        randomColorIndex = 4;
    }

    public Color getOverrideColor() {
        return overrideColor;
    }

    public void setOverrideColor(Color color) {
        this.overrideColor = color;
    }

    protected BufferedImage getFrame() {
        return frame;
    }

    protected void setFrame(int frameIndex) {
        collider.setImage(this.frame = frames[frameIndex]);
    }

    public boolean isUseOverrideColor() {
        return useOverrideColor;
    }

    protected void setUseOverrideColor(boolean useComposite) {
        this.useOverrideColor = useComposite;
    }

    public boolean isAutomaticRandomColorUpdate() {
        return automaticRandomColorUpdate;
    }

    protected void setAutomaticRandomColorUpdate(boolean automaticRandomColorUpdate) {
        this.automaticRandomColorUpdate = automaticRandomColorUpdate;
    }

    public double getRandomColorIndex() {
        return randomColorIndex;
    }

    protected void setRandomColorIndex(double randomColorIndex) {
        this.randomColorIndex = randomColorIndex;
    }

    protected boolean isUseXorColor() {
        return useXorColor;
    }

    protected void setUseXorColor(boolean useXorColor) {
        this.useXorColor = useXorColor;
    }

    public Color getXorColor() {
        return xorColor;
    }

    protected void setXorColor(Color xorColor) {
        this.xorColor = xorColor;
    }

    protected BufferedImage[] getFrames() {
        return frames;
    }

    public void setFrames(BufferedImage[] frames) {
        this.frames = frames;
    }

    public boolean isDrawMask() {
        return drawMask;
    }

    public void setDrawMask(boolean drawMask) {
        this.drawMask = drawMask;
    }

    public boolean isAffectedByMask() {
        return affectedByMask;
    }

    protected void setAffectedByMask(boolean affectedByMask) {
        this.affectedByMask = affectedByMask;
    }

    protected boolean isMaskInverted() {
        return maskInverted;
    }

    protected void setMaskInverted(boolean maskInverted) {
        this.maskInverted = maskInverted;
    }

    public PixelCollider getCollider() {
        return collider;
    }

    public void setCollider(PixelCollider collider) {
        this.collider = collider;
    }
   
    public void drawMask(Graphics2D g) {
    }

    void internalDrawExplosions(Graphics2D g) {
        Composite originalComposite = g.getComposite();
        g.setComposite(composite);
        drawExplosions(g);
        g.setComposite(originalComposite);
    }
    
    protected void drawExplosions(Graphics2D g) {
    }
    
    @Override
    protected void internalDraw(Graphics2D g) {
        if (!isVisible()) {
            return;
        }
        Composite originalComposite = g.getComposite();
        g.setComposite(composite);
        draw(g);
        g.setComposite(originalComposite);
    }

    @Override
    protected void draw(Graphics2D g) {
        if (frame != null) {
            g.drawImage(frame, (int) getX(), (int) getY(), null);
        }
    }
    
    protected void loadFrames(String... resources) {
        frames = new BufferedImage[resources.length];
        int frameIndex = 0;
        for (String resource : resources) {
            resource = "res/" + resource.substring(4); //HACK
            try {
                frames[frameIndex++] = ImageIO.read(MissileCommandGame.class.getResourceAsStream(resource));
            } catch (IOException ex) {
                Logger.getLogger(MissileCommandActor.class.getName()).log(Level.SEVERE, null, ex);
                System.exit(-1);
            }
        }
        setFrame(0);
    }

    private class MCComposite implements Serializable, Composite {
        
        @Override
        public CompositeContext createContext(ColorModel srcColorModel, ColorModel dstColorModel, RenderingHints hints) {
            return new MCCompositeContext();
        }
        
        private class MCCompositeContext implements CompositeContext {
            
            final int[] srcData = new int[4];
            final int[] dstData = new int[4];
            final int[] finalData = new int[4];

            @Override
            public void dispose() {
            }

            @Override
            public void compose(Raster src, Raster dst, WritableRaster dstOut) {
                final int h = dst.getHeight();
                final int w = dst.getWidth();

                BufferedImage mask = game.getMask();
                finalData[3] = 255;
                for (int y = 0; y < h; y++) {
                    for (int x = 0; x < w; x++) {
                        if (affectedByMask && mask.getRGB((x - dst.getSampleModelTranslateX()), (y - dst.getSampleModelTranslateY())) != (maskInverted ? 0xFF000000 : 0xFFFFFFFF))
                            continue;

                        src.getPixel(x, y, srcData);
                        dst.getPixel(x, y, dstData);
                        if (srcData[0] == 0 && srcData[1] == 0 && srcData[2] == 0)
                            continue;

                        if (useOverrideColor) {
                            finalData[0] = combineWithXor(overrideColor.getRed(), dstData[0], xorColor.getRed());
                            finalData[1] = combineWithXor(overrideColor.getGreen(), dstData[1], xorColor.getGreen());
                            finalData[2] = combineWithXor(overrideColor.getBlue(), dstData[2], xorColor.getBlue());
                        } else {
                            finalData[0] = combineWithXor(srcData[0], dstData[0], xorColor.getRed());
                            finalData[1] = combineWithXor(srcData[1], dstData[1], xorColor.getGreen());
                            finalData[2] = combineWithXor(srcData[2], dstData[2], xorColor.getBlue());
                        }
                        dstOut.setPixel(x, y, finalData);
                    }
                }
            }
            
            private int combineWithXor(int srcColor, int dstColor, int xorColor) {
                return useXorColor ?
                    srcColor ^ dstColor ^ xorColor :
                    srcColor;
            }
        }
    }    
    
    // TODO replace with more generic collision detection method later
    boolean checkCollidesWithExplosion(Explosion explosion) {
        double circleDistanceX = Math.abs(explosion.getX() - (getX() + 2));
        double circleDistanceY = Math.abs(explosion.getY() - (getY() + 2));

        BufferedImage f = frame;
        double rectWidthHalf = (f.getWidth() - 4)/2.0;
        double rectHeightHalf = (f.getHeight() - 4)/2.0;

        double rad = explosion.getCurrentRadius();
        if (circleDistanceX > (rectWidthHalf + rad) || circleDistanceY > (rectHeightHalf + rad))
            return false;
        if (circleDistanceX <= rectWidthHalf || circleDistanceY <= rectHeightHalf)
            return true;

        double cornerDistance_sq =
            sqr(circleDistanceX - rectWidthHalf) +
            sqr(circleDistanceY - rectHeightHalf);

        return (cornerDistance_sq <= sqr(rad));
    }

    protected static double sqr(double x) {
        return x*x;
    }

    public boolean checkPointCollision(int sx, int sy) {
        return collider.isInside(sx, sy);
    }
    
    public void onCollision() {
    }

    @Override
    public void update() {
        if (automaticRandomColorUpdate)
            updateRandomColor();

        switch (game.getState()) {
            case INITIALIZING -> updateInitializing();
//            case OL_PRESENTS -> updateOLPresents();
//            case INTRO -> updateIntro();
//            case TITLE -> updateTitle();
            case READY -> updateReady();
            case PLAYING -> updatePlaying();
            case LEVEL_CLEARED -> updateLevelCleared();
            case GAME_OVER -> updateGameOver();
        }
    }

    protected void updateInitializing() {
    }

    protected void updateOLPresents() {
    }

    protected void updateIntro() {
    }

    protected void updateTitle() {
    }

    protected void updateReady() {
    }

    protected void updatePlaying() {
    }

    protected void updateLevelCleared() {
    }

    protected void updateGameOver() {
    }
    
    private void updateRandomColor() {
        randomColorIndex = (randomColorIndex + 0.2) % game.getColors().length;
        overrideColor = game.getColors()[(int) randomColorIndex];
    }        
    
    // --- broadcast messages ---
        
    public void stateChanged() {
    }    
    
}