package br.ol.mc.infra;

/**
 * Time class.
 * 
 * @author Leonardo Ono (ono.leo@gmail.com)
 */
class Time {

    public static long delta;
    private static int fps;

    private static int fpsCount;
    private static long fpsTime;
    private static long lastTime = System.currentTimeMillis();

    public static void update() {
        long currentTime = System.currentTimeMillis();
        delta = currentTime - lastTime;
        fpsTime += delta;
        if (fpsTime > 1000) {
            fps = fpsCount;
            fpsTime = fpsCount = 0;
        } else {
            fpsCount++;
        }
        lastTime = currentTime;
    }

}