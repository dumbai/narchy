package br.ol.mc.infra;

import java.awt.*;

/**
 * Actor class.
 * 
 * @author Leonardo Ono (ono.leo@gmail.com)
 */
public class Actor<T extends Game> implements Comparable<Actor> {
    
    protected final T game;
    private double x, y;
    private double minX = -Double.MAX_VALUE, maxX = Double.MAX_VALUE;
    private double minY = -Double.MAX_VALUE, maxY = Double.MAX_VALUE;
    private boolean visible;
    private boolean updatebleJustWhenVisible = true;
    
    protected int instructionPointer;
    protected long waitTime;

    private int zorder;
    
    public Actor(T game) {
        this.game = game;
    }
    
    public void init() {
    }
    
    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    protected void setX(double x) {
        this.x = x;
        limitMovement();
    }

    protected void setY(double y) {
        this.y = y;
        limitMovement();
    }
    
    protected void set(double x, double y) {
        this.x = x;
        this.y = y;
        limitMovement();
    }

    protected void translate(double x, double y) {
        this.x += x;
        this.y += y;
        limitMovement();
    }

    public double getMinX() {
        return minX;
    }

    public double getMaxX() {
        return maxX;
    }

    public double getMinY() {
        return minY;
    }

    public double getMaxY() {
        return maxY;
    }
    
    public void limitX(double minX, double maxX) {
        this.minX = minX;
        this.maxX = maxX;
    }

    public void limitY(double minY, double maxY) {
        this.minY = minY;
        this.maxY = maxY;
    }
    
    private void limitMovement() {
        x = x < minX ? minX : x;
        x = x > maxX ? maxX : x;
        y = y < minY ? minY : y;
        y = y > maxY ? maxY : y;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isUpdatebleJustWhenVisible() {
        return updatebleJustWhenVisible;
    }

    protected void setUpdatebleJustWhenVisible(boolean updatebleJustWhenVisible) {
        this.updatebleJustWhenVisible = updatebleJustWhenVisible;
    }

    void internalUpdate() {
        if (!updatebleJustWhenVisible || visible) {
            update();
        }
    }

    public int getZorder() {
        return zorder;
    }

    protected void setZorder(int zorder) {
        this.zorder = zorder;
    }
    
    protected void update() {
    }
    
    
    protected void internalDraw(Graphics2D g) {
        if (visible) {
            draw(g);
        }
    }
    
    protected void draw(Graphics2D g) {
    }

    // --- z order ---
    
    @Override
    public int compareTo(Actor o) {
        return this.zorder - o.zorder;
    }
    
}