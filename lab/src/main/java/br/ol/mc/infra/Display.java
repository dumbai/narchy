package br.ol.mc.infra;

import java.awt.*;
import java.awt.image.BufferStrategy;

import static java.awt.RenderingHints.*;

/**
 * Display class.
 * 
 * @author Leonardo Ono (ono.leo@gmail.com)
 */
public class Display extends Canvas {
   
    private final Game game;
    private boolean running;
    private BufferStrategy bs;
    
    public Display(Game game) {
        this.game = game;
        int sx = (int) (game.screenSize.width * game.screenScale.getX());
        int sy = (int) (game.screenSize.height * game.screenScale.getY());
        setPreferredSize(new Dimension(sx, sy));
//        addKeyListener(new Keyboard());
        Mouse mouse = new Mouse(game);
        addMouseListener(mouse);
        addMouseMotionListener(mouse);
    }

    public void start() {
        if (running) {
            return;
        }
        createBufferStrategy(3);
        bs = getBufferStrategy();
        game.init();
        running = true;
        Thread thread = new Thread(new MainLoop());
        thread.start();
    }
    
    private class MainLoop implements Runnable {

        @Override
        public void run() {
            long desiredFrameRateTime = 1000 / 60;
            long currentTime = System.currentTimeMillis();
            long lastTime = currentTime - desiredFrameRateTime;
            long unprocessedTime = 0;
            boolean needsRender = false;
            while (running) {
                currentTime = System.currentTimeMillis();
                unprocessedTime += currentTime - lastTime;
                lastTime = currentTime;
                
                while (unprocessedTime >= desiredFrameRateTime) {
                    unprocessedTime -= desiredFrameRateTime;
                    Time.update();
                    Time.delta = desiredFrameRateTime;
                    update();
                    needsRender = true;
                }
                
                if (needsRender) {
                    Graphics2D g = (Graphics2D) bs.getDrawGraphics();
                    //g.setRenderingHint(RenderingHints.KEY_RENDERING, VALUE_RENDER_QUALITY);
                    g.setRenderingHint(KEY_ANTIALIASING, VALUE_ANTIALIAS_ON);
                    g.setRenderingHint(KEY_ALPHA_INTERPOLATION, VALUE_ALPHA_INTERPOLATION_QUALITY);
                    g.setBackground(Color.BLACK);
                    g.clearRect(0, 0, getWidth(), getHeight());
                    g.scale(game.screenScale.getX(), game.screenScale.getY());
                    draw(g);
                    g.dispose();
                    bs.show();
                    needsRender = false;
                }
                else {
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException ex) {
                    }
                }
            }
        }
        
    }
    
    private void update() {
        game.update();
    }
    
    private void draw(Graphics2D g) {
        game.draw(g);
    }
    
}