package br.ol.mc.infra;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 * Game class.
 * 
 * @author Leonardo Ono (ono.leo@gmail.com)
 */
public class Game {
    
    public final Dimension screenSize = new Dimension(256, 231);
    public final Point2D screenScale = new Point2D.Double(4, 4);
    
    protected final List<Actor> actors = new ArrayList<>();
    private final List<Actor> actorsAdd = new ArrayList<>();
    private final List<Actor> actorsRemove = new ArrayList<>();

    private static final BitmapFontRenderer bitmapFontRenderer =
        new BitmapFontRenderer("./res/font8x8.png", 16, 16);

    public void init() {
    }
    
    public void addActor(Actor a) {
        actorsAdd.add(a);
        a.init();
    }

    public void removeActor(Actor actor) {
        actorsRemove.add(actor);
    }
    
    public void update() {
        for (Actor actor : actors)
            actor.internalUpdate();

        if (actors.removeAll(actorsRemove))
            actorsRemove.clear();
        if (actors.addAll(actorsAdd)) {
            actorsAdd.clear();
            actors.sort(null); //Collections.sort(actors);
        }
    }
    
    public void draw(Graphics2D g) {
        for (Actor actor : actors) actor.internalDraw(g);
    }

    protected void broadcastMessage(String message) {
        for (Actor obj : actors) {
            try {
                obj.getClass().getMethod(message).invoke(obj);
            } catch (Exception ex) {
            }
        }
    }
    
    public static void drawText(Graphics2D g, String text, int x, int y, Color overrideColor) {
        bitmapFontRenderer.drawText(g, text, x, y, overrideColor);
    }
    
}