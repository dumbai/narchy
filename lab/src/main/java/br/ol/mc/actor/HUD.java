package br.ol.mc.actor;

import br.ol.mc.MissileCommandActor;
import br.ol.mc.MissileCommandGame;
import br.ol.mc.MissileCommandGame.State;
import br.ol.mc.infra.Game;

import java.awt.*;

/**
 * HUD class.
 * 
 * @author Leonardo Ono (ono.leo@gmail.com)
 */
public class HUD extends MissileCommandActor {
    
    private final int[] missileAlliesPositions = {8, 114, 224};
    private boolean showArrowLeft;
    
    public HUD(MissileCommandGame game) {
        super(game);
    }

    @Override
    public void init() {
        loadFrames("/res/arrow_left.png");
        setUseOverrideColor(false);
        setUpdatebleJustWhenVisible(false);
        setZorder(1000);
    }

    @Override
    public void updateReady() {
        updateArrowLeft();
    }
    
    @Override
    public void updatePlaying() {
        updateArrowLeft();
    }

    @Override
    public void updateLevelCleared() {
        updateArrowLeft();
    }

    private void updateArrowLeft() {
        showArrowLeft = ((int) (System.nanoTime() * 0.00000001)) % 10 > 2;        
    }
    
    @Override
    protected void draw(Graphics2D g) {
        Game.drawText(g, game.getScoreStr(), 16, 0, game.getFontColor1());
        Game.drawText(g, game.getHiscoreStr(), 88, 0, game.getFontColor1());
        
        if (game.getState() == State.PLAYING) {
            for (int i = 0; i < 3; i++) {
                int availableMissilesAlliesCount = game.getAvailableMissilesAlliesCount(i);
                if (availableMissilesAlliesCount == 0) {
                    Game.drawText(g, "OUT", missileAlliesPositions[i], 223, game.getFontColor2());
                }
                else if (availableMissilesAlliesCount < 4) {
                    Game.drawText(g, "LOW", missileAlliesPositions[i], 223, game.getFontColor2());
                }
            }
        }
        if (showArrowLeft) {
            g.drawImage(getFrame(), 72, 0, null);
        }
    }

    @Override
    public void drawMask(Graphics2D g) {
        if (game.getState() == State.GAME_OVER) {
            Game.drawText(g, game.getScoreStr(), 16, 0, Color.WHITE);
        }
    }
    
    // broadcast messages
    
    @Override
    public void stateChanged() {
        setVisible(
                //game.getState() == State.TITLE ||
             game.getState() == State.READY
            || game.getState() == State.PLAYING
            || game.getState() == State.LEVEL_CLEARED);
        showArrowLeft = game.getState() == State.READY
            || game.getState() == State.PLAYING
            || game.getState() == State.LEVEL_CLEARED;
    }
    
}