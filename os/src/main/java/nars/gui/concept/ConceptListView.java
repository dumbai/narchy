package nars.gui.concept;

import jcog.pri.PriReference;
import nars.Task;
import nars.Term;
import nars.focus.Focus;
import nars.gui.AbstractTaskView;
import spacegraph.space2d.Surface;
import spacegraph.space2d.widget.button.PushButton;
import spacegraph.space2d.widget.text.VectorLabel;

public class ConceptListView extends AbstractTaskView<Term> {

	/** TODO add switch between Tasks and Links */

	public ConceptListView(Focus f, int capacity) {
		super(f.tasks, capacity, f);
	}

	@Override
	protected Term transform(Task x) {
		return x.term().concept();
	}

	@Override
	public Surface apply(int x, int y, PriReference<Term> value) {
		return new PushButton(new VectorLabel(value.get().toString()));
	}
}