package nars.gui;

import jcog.pri.PLink;
import jcog.pri.PriReference;
import jcog.pri.bag.util.Bagregate;
import jcog.pri.op.PriMerge;
import nars.Task;
import nars.focus.Focus;
import nars.task.NALTask;
import spacegraph.space2d.container.ContainerSurface;
import spacegraph.space2d.container.ScrollXY;
import spacegraph.space2d.container.grid.GridRenderer;
import spacegraph.space2d.container.grid.ListModel;
import spacegraph.space2d.container.unit.MutableUnitContainer;

public abstract class AbstractTaskView<X> extends MutableUnitContainer implements GridRenderer<PriReference<X>> {

	private final Bagregate<X> bag;
	private final ScrollXY<PriReference<X>> scroll;
	private final ListModel.AsyncListModel model;
	private final Iterable in;

	protected AbstractTaskView(Iterable tasks, int capacity, Focus focus) {
		this.in = tasks;

		int internalCapacity = capacity * 2; //TODO adaptive

		this.bag = new Bagregate<>(internalCapacity, PriMerge.max);

		scroll = new ScrollXY(model = new ListModel.AsyncListModel(bag, capacity), this);

		set(NARui.get(scroll, this::commit, focus.nar) );
	}

	private void commit() {
		if (visible()) {
			for (var p : in) {
				if (p instanceof PLink pp) p = pp.id;
				accept((NALTask)p);
			}
			bag.commit();
			model.update(scroll);
			var c =  scroll.center();
			if (c!=null)
				((ContainerSurface)c).layout(); //HACK force re-render
		} else {
			bag.clear();
		}
	}

	public final void accept(NALTask x) {
		//TODO option for only if visible
		if (filter(x))
            bag.put(transform(x), value(x));
	}

	public float value(Task x) {
		//TODO optional temporal relevance
		return x.priElseZero();
	}

	protected abstract X transform(Task x);

	protected static boolean filter(Task x) {
		return true;
	}

}