package nars.io;

import nars.NARPart;
import nars.Term;

public abstract class Device extends NARPart {

    protected Device(Term id) {
        super(id);
    }

}